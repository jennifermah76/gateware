puts "TCL_BEGIN: [info script]"

# 
# Save the current working because we're moving to the HLS module directory.
#
set cwd [pwd]
set hlsModuleDir [file normalize $::SMARTHLS]
cd $hlsModuleDir

# 
# Detect where SmartHLS and bash interpreter are located
#
set shls_path [getHlsPath]

#
# Call SmartHLS.
#
# - The Makefile will generate and compile the RISC-V software binary and 
#   the Verilog hardware and integration TCL files (used below).
# - The file open command is just to pipe stdout as SmartHLS compilation advances
set fid [open "| shls -a soc_sw_compile_accel" r]
while {[gets $fid line] != -1} { puts $line }
close $fid
#
# Integrate SmartHLS hardware modules into the Libero project
#
source $hlsModuleDir/hls_output/scripts/shls_integrate_accels.tcl

#
# Restore the working directory
#
cd $cwd

puts "TCL_END: [info script]"
